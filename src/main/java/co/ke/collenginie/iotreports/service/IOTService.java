package co.ke.collenginie.iotreports.service;

import co.ke.collenginie.iotreports.model.Request;
import co.ke.collenginie.iotreports.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class IOTService {
    private Logger LOGGER = LoggerFactory.getLogger(IOTService.class);

    @Autowired
    private RestTemplate restTemplate;

    public Response getReports(Request request) {
        ResponseEntity<Response> response = restTemplate.postForEntity("http://ms-iot-sim-usage-reports.apps.devocp.safaricom.net/api/v1/getDataUsagePerSim", request, Response.class);
        LOGGER.info(response.toString());
        return response.getBody();
    }
}
