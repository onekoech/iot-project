package co.ke.collenginie.iotreports.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Request {
    private String msisdn;
}
