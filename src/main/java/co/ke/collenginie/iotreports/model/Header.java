package co.ke.collenginie.iotreports.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Header {

    private String requestRefId;
    private int responseCode;
    private String responseMessage;
    private String customerMessage;
    private String timestamp;

}
