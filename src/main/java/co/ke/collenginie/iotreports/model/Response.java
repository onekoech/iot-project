package co.ke.collenginie.iotreports.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Response {
    private Header header;
    private List<Body> body;
}
