package co.ke.collenginie.iotreports.model;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Body {


    private String date;
    private String servingNetwork;
    private String apnName;
    private String dataDownload;
    private String dataUpload;
    private String deviceValue;
    private String msisdn;
    private String deviceId;
    private String totalDataVolume;
}
