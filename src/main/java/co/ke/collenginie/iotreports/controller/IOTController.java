package co.ke.collenginie.iotreports.controller;


import co.ke.collenginie.iotreports.model.Request;
import co.ke.collenginie.iotreports.model.Response;
import co.ke.collenginie.iotreports.service.IOTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/iot")
public class IOTController {

    @Autowired
    private IOTService iotService;

    @GetMapping("report")
    private Response getReports(@RequestBody Request request){
        return iotService.getReports(request);
    }
}
